package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

const (
	bindAddress         = "0.0.0.0:8080"
	responseDelay       = time.Millisecond * 500
	shutdownGracePeriod = time.Second * 5
)

var (
	hostname, hasHostname = os.LookupEnv("HOSTNAME")
	concurrency int64 = 0
	concurrencyMutex sync.Mutex
)

func addConcurrency(d int64) {
	concurrencyMutex.Lock()
	defer concurrencyMutex.Unlock()
	concurrency += d
}

func server(ctx context.Context) error {
	res := "Hello World"
	if hasHostname {
		res = fmt.Sprintf("Hello from %q\n", hostname)
	}

	srv := http.Server{
		Addr: bindAddress,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/metrics" {
				w.Header().Add("Content-Type", "text/plain; version=0.0.4; charset=utf-8; escaping=values")
				w.Header().Add("Transfer-Encoding", "chunked")
				w.WriteHeader(http.StatusOK)
				fmt.Fprintf(w, "http_concurrency %d\n", concurrency)
				return
			}

			addConcurrency(1)
			defer addConcurrency(-1)

			time.Sleep(responseDelay)
			fmt.Fprint(w, res)
		}),
	}

	errCh := make(chan error, 1)
	go func() {
		log.Printf("Listening on http://%s", bindAddress)
		errCh <- srv.ListenAndServe()
	}()

	select {
	case err := <-errCh:
		return err
	case <-ctx.Done():
		log.Println("Shutting down")
		ctx, cancel := context.WithTimeout(context.Background(), shutdownGracePeriod)
		defer cancel()
		return srv.Shutdown(ctx)
	}
}

func client(ctx context.Context, numWorkersStr string) error {
	numWorkers, err := strconv.ParseUint(numWorkersStr, 10, 64)
	if err != nil {
		return err
	}

	wg := sync.WaitGroup{}
	client := http.Client{
		Transport: &http.Transport{
			MaxIdleConns: 0,
			MaxIdleConnsPerHost: 0,
			MaxConnsPerHost: 0,
			ForceAttemptHTTP2: true,
		},
	}

	for i := uint64(0); i < numWorkers; i++ {
		wg.Add(1)
		go func(i uint64){
			defer wg.Done()
			for {
				req, err := http.NewRequest(http.MethodGet, "http://backend.127.0.0.1.nip.io", nil)
				if err != nil {
					panic(err)
				}
				req = req.WithContext(ctx)
				res, err := client.Do(req)
				if err != nil {
					panic(err)
				}
				if res.StatusCode != http.StatusOK {
					b, _ := io.ReadAll(res.Body)
					log.Printf("Worker %d made request, got non-200 status: %d: %s", i, res.StatusCode, strings.ReplaceAll(string(b), "\n", " "))
				} else {
					log.Printf("Worker %3d made request", i)
				}
			}
		}(i)
	}

	wg.Wait()
	return nil
}

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	var err error
	if len(os.Args) == 2 {
		err = client(ctx, os.Args[1])
	} else {
		err = server(ctx)
	}

	if err != nil {
		log.Fatal(err)
	}
}
