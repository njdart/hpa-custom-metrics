apiVersion: apps/v1
kind: Deployment
metadata:
    name: backend
    labels:
        app.kubernetes.io/name: backend
spec:
    selector:
        matchLabels:
            app.kubernetes.io/name: backend
    strategy:
        type: RollingUpdate
        rollingUpdate:
            maxSurge: 1
            maxUnavailable: 0
    template:
        metadata:
            labels:
                app.kubernetes.io/name: backend
            annotations:
                prometheus.io/scrape: "true"
                prometheus.io/port: "8080"
        spec:
            containers:
                - name: golang
                  image: gcr.io/distroless/static-debian12
                  command:
                      - /go/src/gitlab.com/njdart/hpa-custom-metrics/tmp/main
                  workingDir:
                  ports:
                      - containerPort: 8080
                  volumeMounts:
                      - mountPath: /go/src/gitlab.com/njdart/hpa-custom-metrics
                        name: workdir
                        readOnly: true
                  env:
                      - name: GOMEMLIMIT
                        valueFrom:
                            resourceFieldRef:
                                resource: requests.memory
                  resources:
                      requests:
                          memory: 90Mi
                          cpu: 50m
                      limits:
                          memory: 100Mi
                          cpu: 100m
            volumes:
                - name: workdir
                  hostPath:
                      path: /work_dir
                      type: Directory # Must exist before hand, won't be created by k8s
---
apiVersion: v1
kind: Service
metadata:
    name: backend
spec:
    selector:
        app.kubernetes.io/name: backend
    ports:
        - protocol: TCP
          port: 8080
          targetPort: 8080
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: backend
spec:
    ingressClassName: nginx
    rules:
        - host: backend.127.0.0.1.nip.io
          http:
              paths:
                  - path: /
                    pathType: Prefix
                    backend:
                        service:
                            name: backend
                            port:
                                number: 8080
---
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
    name: backend
spec:
    scaleTargetRef:
        apiVersion: apps/v1
        kind: Deployment
        name: backend
    minReplicas: 1
    maxReplicas: 10
    metrics:
        ## These are commented out purely to make `kubectl get hpa backend -w` easier to read
        ## having too many HPA values causes HPA targets to be truncated
        # - type: Resource
        #   resource:
        #       name: cpu
        #       target:
        #           type: Utilization
        #           averageUtilization: 75
        # - type: Resource
        #   resource:
        #       name: memory
        #       target:
        #           type: Utilization
        #           averageUtilization: 75

        # use a "Pods" metric, which takes the average of the
        # given metric across all pods controlled by the autoscaling target
        - type: Pods
          pods:
              # use the metric that you used above: pods/http_requests
              metric:
                  name: http_concurrency
              # target 5 concurrent requests
              target:
                  type: Value
                  averageValue: 5
