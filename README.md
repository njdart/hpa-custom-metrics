# HPA Custom Metrics Autoscaler

An example of using a custom prometheus metric (`http_concurrency`) collected from
pods in a deployment as a heuristic in the Horizontal-Pod-Autoscaler.

## Quickstart

1.  Requirements

    -   Golang 1.22+
    -   `docker`
    -   [Kind](https://kind.sigs.k8s.io)
    -   `kubectl`

1.  Start a local Kubernets cluster with `kind` using the quick-start script
    `./kind-create-cluster.sh`. This will do a few things

    -   Build the go project into `tmp/main` (you can run [air](https://github.com/cosmtrek/air) locally to auto-build on change. Note, if running on ARM architectures, you may have difficulty using this as the container requires `GOOS=linux` but native builds output `GOOS=darwin`)
    -   Create the cluster with a local volume mount so the go program here can be easily
        run and updated in a pod without having to rebuild the container image. See `kind-cluster.yaml`
    -   Deploy the contents of `kind-apps.yaml` which provision a Prometheus + Grafana stack for collecting metrics,
        a kube-metric- and kube-state- servers, an ingress controller, and a custom metrics adapter to allow K8s to query
        prometheus for metrics.
    -   Deploy the test application from `my-apps.yaml`
    -   A bunch of waiting in between whilst kind tries its darnedest under all this pressure. It was literally bourn a few seconds ago!

    Once the `kind-create-cluster.sh` script is done, you should have everything deployed and running. Open two terminals and run

    -   `kubectl get hpa backend -w` to watch the HPA as it decides to scale up and down
    -   `go run . 32` to spin up 32 concurrent requests to the "backend" - the HPA settings will try to keep this at about 5
        concurrent requests per pod based on the collected metrics.

    Example:

    ```
    $ ./kind-create-cluster.sh
    Creating cluster "kind" ...
    ✓ Ensuring node image (kindest/node:v1.29.2) 🖼
    ✓ Preparing nodes 📦
    ✓ Writing configuration 📜
    ✓ Starting control-plane 🕹️
    ✓ Installing CNI 🔌
    ✓ Installing StorageClass 💾
    Set kubectl context to "kind-kind"
    You can now use your cluster with:

    ...

    Prometheus can be viewed at http://prometheus.127.0.0.1.nip.io/
    Grafana can be viewed at http://grafana.127.0.0.1.nip.io/ with creds admin:admin
    The sample app can be viewed at http://backend.127.0.0.1.nip.io/
    ```

## References

-   https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/#autoscaling-on-multiple-metrics-and-custom-metrics
-   https://github.com/kubernetes-sigs/prometheus-adapter/blob/master/docs/walkthrough.md
-   https://github.com/kubernetes-sigs/prometheus-adapter/blob/master/docs/config-walkthrough.md
-   https://github.com/prometheus-operator/prometheus-operator?tab=readme-ov-file#quickstart
