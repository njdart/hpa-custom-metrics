#!/bin/sh
set -o errexit

mkdir -p ./tmp && CGO_ENABLED=0 GOOS=linux go build -o tmp/main .

kind create cluster --config=./kind-cluster.yaml

echo "Waiting for kind to warm up and become ready"
kubectl wait --namespace kube-system --for=condition=ready pod --selector=component=kube-apiserver --timeout=90s
kubectl wait --namespace kube-system --for=condition=ready pod --selector=component=kube-controller-manager --timeout=90s
kubectl wait --namespace kube-system --for=condition=ready pod --selector=component=kube-scheduler --timeout=90s
kubectl wait --namespace kube-system --for=condition=ready pod --selector=component=etcd --timeout=90s

echo "Deploying metric-server, nginx-ingress, prometheus, grafana"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml
kubectl patch -n kube-system deployment metrics-server --type=json -p '[{"op":"add","path":"/spec/template/spec/containers/0/args/-","value":"--kubelet-insecure-tls"}]'
# Unfortunatly, we need to wait for this to be done before we can deploy our kind-apps
kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=300s
kubectl apply -f ./kind-apps.yaml
kubectl apply -f ./my-apps.yaml

# Give kind some time to catch up
sleep 5

kubectl wait --namespace monitoring --for=condition=ready pod --selector=app=prometheus --timeout=90s
kubectl wait --namespace monitoring --for=condition=ready pod --selector=app=grafana --timeout=90s
kubectl wait --namespace monitoring --for=condition=ready pod --selector=app.kubernetes.io/name=prometheus-adapter --timeout=90s

echo
echo "Prometheus can be viewed at http://prometheus.127.0.0.1.nip.io/"
echo "Grafana can be viewed at http://grafana.127.0.0.1.nip.io/ with creds admin:admin"
echo "The sample app can be viewed at http://backend.127.0.0.1.nip.io/"
